<?php
include 'get.php';
?>

    <html>

    <head>
        <title>Workstation Editor</title>
        <link href="css/editor_main.css" rel="stylesheet">
        <link rel="stylesheet" href="lib/font-awesome-4.4.0/css/font-awesome.min.css">
        <link href="css/editor.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="img/favicon.ico">
    </head>

    <body ng-app="app" ng-controller="main">
        <div class="header">
            <span>WORKSTATION EDITOR</span>
            <img src="img/terumo-nav-logo.png">
        </div>
        <div class="alert alert-info customAlert" customalert>
            <span ng-bind-html="customAlertMessage"></span>
        </div>
        <div class="outerWrapper">
            <div class="pull-right">
                <button class="btn btn-primary fontUp" ng-click="addGroup()">ADD GROUP <i class="fa fa-plus"></i></button>
                <button class="btn btn-success fontUp" ng-click="submit()">SAVE EVERYTHING <i class="fa fa-arrow-right"></i></button>
            </div>
            <div style="clear:both;"></div>
            <div class="blackout" ng-class="{blackoutShow:showTab}">
                <div class="prompt">
                    <div class="inner">
                        <span class="promptTitle">Edit Tab</span>
                        <button class="btn btn-danger pull-right" ng-click="deleteTab()"><i class="fa fa-trash"></i> DELETE TAB</button>
                        <hr>
                        <label for="currentTabTitle">Title:</label>
                        <input class="form-control currentTabTitle" id="currentTabTitle" ng-model="currentTab.title" />
                        <br>
                        <label for="editButtons">Buttons:</label>
                        <button class="btn btn-primary pull-right" ng-click="addButton(currentTab.buttons)">ADD BUTTON <i class="fa fa-plus"></i></button>
                        <div style="clear:both"></div>
                        <ng-pluralize count="currentTab.buttons.length" when="{'one': 'There is 1 button in this tab','other': 'There are {} buttons in this tab'}"></ng-pluralize>
                        <div>
                            <div id="editButtons" class="assetBtn editButtons" ng-repeat="button in currentTab.buttons" style="background:{{button.color}}" ng-click="editButton(button,$index)">
                                <span>{{button.title}}</span>
                                <div class="icon">
                                    <img width="40" border="0" height="40" ng-src="img/icons/icon-{{button.icon}}.png">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="pull-right">
                            <button class="btn btn-warning" ng-click="showTab=false"><i class="fa fa-undo"></i> CANCEL</button>
                            <button class="btn btn-success" ng-click="saveTab(currentTab)">DONE <i class="fa fa-check"></i></button>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>
            <div class="blackout" ng-class="{blackoutShow:showAddButton}">
                <div class="prompt">
                    <div class="inner">
                        <span class="promptTitle">Add Button</span>
                        <hr>
                        <div>
                            <div class="inline-block">
                                <label for="title">Title:</label>
                                <input type="text" class="form-control" ng-model="newButton.title" id="title">
                                <br>
                                <label for="icon">Icon:</label>
                                <select class="form-control" id="icon" ng-model="newButton.icon">
                                    <option ng-repeat="i in icons" value="{{i}}">{{i}}</option>
                                </select>
                            </div>
                            <div class="inline-block">
                                <label for="color">Color:</label>
                                <div class="colorPicker" id="color">
                                    <div class="eachColor" ng-repeat="c in colors" style="background:{{c}}" ng-click="changeColor(c,newButton)" ng-class="{'colorActive':newButton.color===c}"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="padding: 0 15px;">
                                <label for="media">Media Type:</label>
                                <div id="media" ng-repeat="m in availableMedia">
                                    <input type="radio" ng-model="newButton.media" ng-value="m" ng-change="clearRadio(newButton)" /> {{m}}
                                </div>
                                <br>
                                <div>
                                    <div ng-show="newButton.media==='Website'">
                                        <label for="site">Website:</label>
                                        <input type="text" class="form-control" ng-model="newButton.mediaLink" id="site">
                                    </div>
                                    <div ng-show="newButton.media==='Video'">
                                        <label for="video">Video:</label>
                                        <select class="form-control" id="video" ng-model="newButton.mediaLink">
                                            <option ng-repeat="v in availableVideos" value="{{v}}">{{v}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="newButton.media==='Virtual Tour'">
                                        <label for="tour">Virtual Tour:</label>
                                        <select class="form-control" id="tour" ng-model="newButton.mediaLink">
                                            <option ng-repeat="t in availableTours" value="{{t}}">{{t}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="newButton.media==='PDF'">
                                        <label for="pdf">PDF:</label>
                                        <select class="form-control" id="pdf" ng-model="newButton.mediaLink">
                                            <option ng-repeat="p in availablePdfs" value="{{p}}">{{p}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="newButton.media==='Other'">
                                        <label for="other">Other:</label>
                                        <input type="text" class="form-control" ng-model="newButton.mediaLink" id="other">
                                    </div>
                                </div>
                            </div>
                            <div class="assetBtn editButton" style="background:{{newButton.color}};">
                                <span>{{newButton.title}}</span>
                                <div class="icon">
                                    <img width="40" border="0" height="40" ng-src="img/icons/icon-{{newButton.icon}}.png">
                                </div>
                            </div>
                            <br>
                            <div class="pull-right">
                                <button class="btn btn-warning" ng-click="showAddButton=false"><i class="fa fa-undo"></i> CANCEL</button>
                                <button class="btn btn-success" ng-click="addNewButtonToGroup(newButton,addButtonGroup)">DONE <i class="fa fa-check"></i></button>
                            </div>
                            <div style="clear:both;"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blackout" ng-class="{blackoutShow:showPrompt}">
                <div class="prompt">
                    <div class="inner">
                        <span class="promptTitle">Edit Button</span>
                        <button class="btn btn-danger pull-right" ng-click="deleteButton()"><i class="fa fa-trash"></i> DELETE BUTTON</button>
                        <div style="clear:both;"></div>
                        <hr>
                        <div>
                            <div class="inline-block">
                                <label for="title">Title:</label>
                                <input type="text" class="form-control" ng-model="currentBtn.title" id="title">
                                <br>
                                <label for="icon">Icon:</label>
                                <select class="form-control" id="icon" ng-model="currentBtn.icon">
                                    <option ng-repeat="i in icons" value="{{i}}">{{i}}</option>
                                </select>
                            </div>
                            <div class="inline-block">
                                <label for="color">Color:</label>
                                <div class="colorPicker" id="color">
                                    <div class="eachColor" ng-repeat="c in colors" style="background:{{c}}" ng-click="changeColor(c,currentBtn)" ng-class="{'colorActive':currentBtn.color===c}"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="padding: 0 15px;">
                                <label for="media">Media Type:</label>
                                <div id="media" ng-repeat="m in availableMedia">
                                    <input type="radio" ng-model="currentBtn.media" ng-value="m" ng-checked="currentBtn.media===m" ng-change="clearRadio(currentBtn)" /> {{m}}
                                </div>
                                <br>
                                <div>
                                    <div ng-show="currentBtn.media==='Website'">
                                        <label for="site">Website:</label>
                                        <input type="text" class="form-control" ng-model="currentBtn.mediaLink" id="site">
                                    </div>
                                    <div ng-show="currentBtn.media==='Video'">
                                        <label for="video">Video:</label>
                                        <select class="form-control" id="video" ng-model="currentBtn.mediaLink">
                                            <option ng-repeat="v in availableVideos" value="{{v}}">{{v}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="currentBtn.media==='Virtual Tour'">
                                        <label for="tour">Virtual Tour:</label>
                                        <select class="form-control" id="tour" ng-model="currentBtn.mediaLink">
                                            <option ng-repeat="t in availableTours" value="{{t}}">{{t}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="currentBtn.media==='PDF'">
                                        <label for="pdf">PDF:</label>
                                        <select class="form-control" id="pdf" ng-model="currentBtn.mediaLink">
                                            <option ng-repeat="p in availablePdfs" value="{{p}}">{{p}}</option>
                                        </select>
                                    </div>
                                    <div ng-show="currentBtn.media==='Other'">
                                        <label for="other">Other:</label>
                                        <input type="text" class="form-control" ng-model="currentBtn.mediaLink" id="other">
                                    </div>
                                </div>
                            </div>
                            <div class="assetBtn editButton" style="background:{{currentBtn.color}};">
                                <span>{{currentBtn.title}}</span>
                                <div class="icon">
                                    <img width="40" border="0" height="40" ng-src="img/icons/icon-{{currentBtn.icon}}.png">
                                </div>
                            </div>
                            <br>
                            <div class="pull-right">
                                <button class="btn btn-warning" ng-click="showPrompt=false"><i class="fa fa-undo"></i> CANCEL</button>
                                <button class="btn btn-success" ng-click="saveButton(currentBtn)">DONE <i class="fa fa-check"></i></button>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blackout" ng-class="{blackoutShow:showImg}">
                <div class="prompt">
                    <div class="inner">
                        <span class="promptTitle">Choose an Image</span>
                        <hr>
                        <div>
                            <img ng-src="{{currentImg.img}}" style="width:100%;height:300px;" />
                        </div>
                        <br>
                        <label for="bgImage">Image:</label>
                        <select class="form-control" id="bgImage" ng-model="currentImg.img">
                            <option ng-repeat="i in availableImgs" value="{{i}}">{{i}}</option>
                        </select>
                        <br>
                        <div class="pull-right">
                            <button class="btn btn-warning" ng-click="showImg=false"><i class="fa fa-undo"></i> CANCEL</button>
                            <button class="btn btn-success" ng-click="saveImg(currentImg)">DONE <i class="fa fa-check"></i></button>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="groups">
                    <div class="eachGroup" ng-repeat="(i,bg) in buttonGroups">
                        <div class="leftTable">
                            <table class="contentTable">
                                <tr>
                                    <td>
                                        Group:
                                    </td>
                                    <td>
                                        <input id="bgTitle" class="bgTitle" ng-model="bg.title" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Image:
                                    </td>
                                    <td>
                                        <img ng-src="{{bg.img}}" class="bgImg" ng-click="editImg(bg)" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rightTable">
                            <div class="pull-right">
                                <button class="btn btn-primary" ng-click="addTab(bg)">ADD TAB TO THIS GROUP <i class="fa fa-plus"></i></button>
                                <button class="btn btn-danger" ng-click="removeGroup(i)"><i class="fa fa-trash"></i> REMOVE GROUP</button>
                            </div>
                            <div style="clear:both;"></div>
                            <ng-pluralize count="bg.tabs.length" when="{'one': 'There is 1 tab in this group','other': 'There are {} tabs in this group'}"></ng-pluralize>
                            <table class="contentTable tabTable" ng-repeat="tab in bg.tabs" ng-click="editTab(bg,tab,$index)">
                                <tr>
                                    <td>
                                        Tab Title:
                                    </td>
                                    <td style="vertical-align:middle;">
                                        {{tab.title}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Buttons:
                                    </td>
                                    <td>
                                        <div class="assetBtn" ng-repeat="button in tab.buttons" style="background:{{button.color}}">
                                            <span>{{button.title}}</span>
                                            <div class="icon">
                                                <img width="40" border="0" height="40" ng-src="img/icons/icon-{{button.icon}}.png">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!--Angular-->
    <script src="lib/angular.min.js"></script>
    <script>
        var phpData = <?php echo $str?>;
        var imgs = <?php echo $img?>;
        var vids = <?php echo $vid?>;
        var vt = <?php echo $vt?>;
        var pdf = <?php echo $pdf?>;
    </script>
    <script src="editor.js"></script>
    <script src="editor-directives.js"></script>

    </html>