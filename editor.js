var app = angular.module('app', []);
app.controller('main', function ($scope, $http) {

    //assign button groups from php
    $scope.buttonGroups = phpData;

    //available images in img/background folder
    $scope.availableImgs = imgs;

    //available videos in videos folder
    $scope.availableVideos = vids;

    //available tours in virtual tours folder
    $scope.availableTours = vt;

    //media types
    $scope.availableMedia = ["Video", "PDF", "Website", "Virtual Tour", "Other"];

    //pdfs
    $scope.availablePdfs = pdf;

    //icon list
    $scope.icons = ['activepres', 'document', 'games', 'presentation', 'video', 'web', 'list'];

    //colors list
    $scope.colors = ["#009a44", "#ff7900", "#a0a0a0", "#76d750", "#fecb00", "#575e63", "#f7403a", "#447ebc", "#aa6dab"];

    //show image prompt
    $scope.showImg = false;

    //current img
    $scope.currentImg = null;

    //image clicked on
    $scope.imageClicked = null;

    //function to edit images and show prompt
    $scope.editImg = function (i) {
        $scope.imageClicked = i;
        $scope.currentImg = angular.copy(i);
        $scope.showImg = true;
    };

    //function to save image
    $scope.saveImg = function (c) {
        $scope.imageClicked.img = c.img;
        $scope.imageClicked = null;
        $scope.currentImg = null;
        $scope.showImg = false;
    };

    //current button
    $scope.currentBtn = null;
    $scope.currentBtnIndex = null;

    //assign clicked button
    $scope.buttonClicked = null;

    //edit button
    $scope.editButton = function (b, i) {
        $scope.buttonClicked = b;
        $scope.currentBtnIndex = i;
        $scope.currentBtn = angular.copy(b);
        $scope.showPrompt = true;
    };

    $scope.clearRadio = function (b) {
        b.mediaLink = '';
    };

    //save new button
    $scope.saveButton = function (cb) {
        $scope.showPrompt = false;
        $scope.buttonClicked.color = cb.color;
        $scope.buttonClicked.title = cb.title;
        $scope.buttonClicked.icon = cb.icon;
        if (cb.media) {
            $scope.buttonClicked.media = cb.media;
            $scope.buttonClicked.mediaLink = cb.mediaLink;
        } else {
            $scope.buttonClicked.media = '';
            $scope.buttonClicked.mediaLink = '';
        }

        $scope.buttonClicked = null;
    };

    //colors
    $scope.changeColor = function (c, b) {
        b.color = c;
    };

    //delete button
    $scope.deleteButton = function () {
        var c = confirm("Are you sure you want to delete this button?");
        if (c) {
            $scope.currentTab.buttons.splice($scope.currentBtnIndex, 1);
            $scope.showPrompt = false;
        }
    };

    //button variables
    $scope.showAddButton = false;
    $scope.newButton = null;
    $scope.addButtonGroup = null;

    //add button
    $scope.addButton = function (b) {
        $scope.addButtonGroup = b;
        $scope.newButton = {
            color: "#009a44",
            title: "New Button",
            icon: "web"
        };

        $scope.showAddButton = true;
    };

    //add new button to group
    $scope.addNewButtonToGroup = function (newButton, buttons) {
        buttons.push(newButton);
        $scope.newButton = null;
        $scope.addButtonGroup = null;
        $scope.showAddButton = false;
    };

    //tab variables
    $scope.showTab = false;
    $scope.currentTab = null;
    $scope.tabClicked = null;

    //current button group
    $scope.currentGroup = null;
    $scope.currentTabIndex = null;

    //edit tab
    $scope.editTab = function (bg, t, i) {
        $scope.showTab = true;
        $scope.tabClicked = t;
        $scope.currentGroup = bg;
        $scope.currentTabIndex = i;
        $scope.currentTab = angular.copy(t);
    };

    //add new tab
    $scope.addTab = function (bg) {
        bg.tabs.push({
            active: bg.tabs.length === 0 ? true : false,
            title: "New Tab",
            buttons: []
        });

        //broadcast
        $scope.$broadcast("showalert", "You have added a new tab to the <strong>BOTTOM</strong> of this group!");
    };

    //save tab
    $scope.saveTab = function (t) {
        $scope.showTab = false;
        $scope.tabClicked.title = t.title;
        $scope.tabClicked.buttons = t.buttons;
        $scope.tabClicked = null;
    };

    //delete tab
    $scope.deleteTab = function (t) {
        var c = confirm("Are you sure you want to delete this tab?");
        if (c) {
            $scope.currentGroup.tabs.splice($scope.currentTabIndex, 1);
            $scope.showTab = false;
        }
    };

    //add group
    $scope.addGroup = function () {
        var length = Object.keys($scope.buttonGroups).length;
        $scope.buttonGroups["buttonGroup" + (length + 1)] = {
            "title": "New Group",
            "img": "img/background/Home-background-1.jpg",
            tabs: []
        };

        //broadcast
        $scope.$broadcast("showalert", "You have added a new group to the <strong>BOTTOM</strong> of the list!");

    };

    //remove group
    $scope.removeGroup = function (g) {
        var c = confirm("Are you sure you want to delete this group?");
        if (c) {
            delete $scope.buttonGroups[g];
        }
    };

    //save json
    $scope.submit = function () {
        $http({
            method: 'POST',
            url: 'save.php',
            data: $scope.buttonGroups,
        }).then(function (response) {
            alert(response.data);
        });
    };
});