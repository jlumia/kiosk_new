function color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

//more tab
var more = [{
    title: "visit terumobct.com",
    color: color(),
    icon: "web"
    }, {
    title: "visit our customer portal",
    color: color(),
    icon: "web"
    }, {
    title: "learn more about our customer support",
    color: color(),
    icon: "activepres"
    }, {
    title: "terumo bct games",
    color: color(),
    icon: "games"
}];

//master JSON
var master = {
    buttonGroup1: {
        title: "Button Group 1",
        tabs: [{
            active: true,
            title: "1A",
            buttons: [
                {
                    title: "1AA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "1AB",
                    color: color(),
                    icon: "document",
                    href: "http://www.pinkbike.com"
                    }
                ]
            }, {
            active: false,
            title: "1B",
            buttons: [
                {
                    title: "1BA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "1BB",
                    color: color(),
                    icon: "video"
                    }
                ]
            }, {
            active: false,
            title: "1C",
            buttons: [
                {
                    title: "1CA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "1CB",
                    color: color(),
                    icon: "video"
                    },
                {
                    title: "1CC",
                    color: color(),
                    icon: "web"
                    }
                ]
            }]
    },
    buttonGroup2: {
        title: "Button Group 2",
        tabs: [{
            active: true,
            title: "2A",
            buttons: [
                {
                    title: "2AA",
                    color: color(),
                    icon: "video"
                    },
                {
                    title: "2AB",
                    color: color(),
                    icon: "web"
                    }
                ]
            }, {
            active: false,
            title: "2B",
            buttons: [
                {
                    title: "2BA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "2BB",
                    color: color(),
                    icon: "document"
                    }
                ]
            }, {
            active: false,
            title: "2C",
            buttons: [
                {
                    title: "2CA",
                    color: color(),
                    icon: "video"
                    },
                {
                    title: "2CB",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "2CC",
                    color: color(),
                    icon: "document"
                    }
                ]
            }]
    },
    buttonGroup3: {
        title: "Button Group 3",
        tabs: [{
            active: true,
            title: "3A",
            buttons: [
                {
                    title: "3AA",
                    color: color(),
                    icon: "video"
                    },
                {
                    title: "3AB",
                    color: color(),
                    icon: "web"
                    }
                ]
            }, {
            active: false,
            title: "3B",
            buttons: [
                {
                    title: "3BA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3BB",
                    color: color(),
                    icon: "web"
                    }
                ]
            }, {
            active: false,
            title: "3C",
            buttons: [
                {
                    title: "3CA",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3CB",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3CC",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3CD",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3CE",
                    color: color(),
                    icon: "web"
                    },
                {
                    title: "3CF",
                    color: color(),
                    icon: "web"
                    }
                ]
            }]
    }
};