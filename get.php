<?php

//get master json
$str = file_get_contents('master.json');


//get images
$files = scandir("img/background");
$f = array_splice($files,2);
foreach ($f as &$i) {
    $i = "img/background/".$i;
}
$img = json_encode($f);

//get videos
$files = scandir("videos");
$f = array_splice($files,2);
foreach ($f as &$i) {
    $i = "videos/".$i;
}
$vid = json_encode($f);

//get virtual tours
$files = scandir("virtual-tours");
$f = array_splice($files,2);
$vt = json_encode($f);

//get pdfs
$files = scandir("pdfs");
$f = array_splice($files,2);
foreach ($f as &$i) {
    $i = "pdfs/".$i;
}
$pdf = json_encode($f);
?>