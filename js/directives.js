app.directive('backgroundcycler', function ($interval, $timeout) {
    return function (scope, element, attrs) {

        //active img counter
        var count = 0;

        //add active class to first image
        angular.element(element.children()[count]).addClass("active");

        //iterate over background images
        $interval(function () {

            //remove active class
            angular.element(element.children()[count]).removeClass("active");

            //iterate
            count++;

            //reset counter if greater than image length
            if (count === element.children().length) {
                count = 0;
            }

            //add new class after 1s
            var timer = $timeout(
                function () {
                    angular.element(element.children()[count]).addClass("active");
                    $timeout.cancel(timer);
                }, 1000);

        }, 10000);
    };
});

app.directive('buttons', function ($interval, $timeout) {
    return function (scope, element, attrs) {
        
        var int = null;
        //hide buttons listener
        scope.$on("hidebuttons", function () {
            angular.element(document.getElementsByClassName("tabs")[0]).addClass("disableClick");
            element.addClass("subHide");
            if (int) {
                $interval.cancel(int);
            }
        });

        scope.$on("showbuttons", function () {
            angular.element(document.getElementsByClassName("tabs")[0]).addClass("disableClick");
            element.removeClass("subHide");

            //show buttons on interval
            var timer = $timeout(function () {
                var count = 0;
                angular.element(element.children()[count]).addClass('showButton');
                var len = element.children().length - 1;
                int = $interval(function () {
                    angular.element(element.children()[count]).addClass('showButton');
                    if (count === len) {
                        $interval.cancel(int);
                        return;
                    }
                    count++;
                }, 333);
                angular.element(document.getElementsByClassName("tabs")[0]).removeClass("disableClick");
                $timeout.cancel(timer);
            }, 500);
        });
    };
});

app.directive('customvideo', function ($timeout) {
    return function (scope, element, attrs) {

        scope.$on('stopvideo', function () {
            element[0].pause();
            var timer = $timeout(function () {
                element[0].load();
                $timeout.cancel(timer);
            }, 1000);
        });
    };
});