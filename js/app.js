var app = angular.module('app', []);

app.controller('main', function ($scope, $timeout, $http, $sce) {

    //assign master obj
    $http({
        method: 'GET',
        url: 'master.json'
    }).then(function (response) {
        $scope.masterObj = response.data;
    });

    //assign more buttons
    $http({
        method: 'GET',
        url: 'more.json'
    }).then(function (response) {
        $scope.moreButtons = response.data;
    });

    //assign corp buttons
    $http({
        method: 'GET',
        url: 'corp.json'
    }).then(function (response) {
        $scope.corpButtons = response.data;
    });

    //current obj/tab
    $scope.currentGroup = null;
    $scope.currentTab = null;
    $scope.currentButton = null;

    $scope.currentView = 0;
    $scope.newView = 0;

    //button to advance into masterObj
    $scope.advance = function (button) {
        if ($scope.currentView === null) {
            return;
        }
        $scope.newView = angular.copy($scope.currentView);
        $scope.currentView = null;
        $scope.currentGroup = button;
        var timer = $timeout(function () {
            $scope.newView++;
            $scope.currentView = $scope.newView;
            $scope.currentTab = button.tabs[0];
            $scope.currentTab.active = true;
            $scope.$broadcast("showbuttons");
            $timeout.cancel(timer);
        }, 1000);
    };

    //button to go back home
    $scope.back = function () {

        //prevent button mashing
        if ($scope.currentView === null) {
            return;
        }

        //trick back to 0
        if ($scope.currentView === 3 || $scope.currentView === 4) {
            $scope.currentView = 1;
        }

        if ($scope.homeBool && $scope.currentView === 2) {
            $scope.currentView = 5;
        }

        //stop video if it exists
        $scope.$broadcast('stopvideo');
        $scope.newView = angular.copy($scope.currentView);
        $scope.currentView = null;

        var timer = $timeout(function () {
            $scope.newView--;
            $scope.currentView = $scope.newView;

            switch ($scope.currentView) {
            case 0:
                angular.forEach($scope.currentGroup.tabs, function (v) {
                    if (v.active) {
                        v.active = false;
                    }
                });
                $scope.homeBool = false;
                $scope.currentGroup = null;
                $scope.currentTab = null;
                break;
            case 3:
            case 4:
                $scope.currentGroup = null;
                $scope.currentTab = null;
                break;
            case 1:
                $scope.currentButton = null;
                break;
            }

            $timeout.cancel(timer);
        }, 1000);
    };

    //show new tab
    $scope.showNew = function (t) {
        if (t === $scope.currentTab) {
            return;
        }
        $scope.$broadcast("hidebuttons");
        $scope.currentTab.active = false;
        t.active = true;
        var timer = $timeout(function () {

            $scope.currentTab = t;
            $scope.$broadcast("showbuttons");
            $timeout.cancel(timer);
        }, 1000);

    };

    $scope.homeBool = false;
    $scope.moreAndHome = function (i, g) {

        if (i === 4) {
            $scope.homeBool = true;
        }

        $scope.currentView = null;
        $scope.currentGroup = {
            img: 'img/background/' + g + '-background.jpg'
        };
        var timer = $timeout(function () {
            $scope.currentView = i;
            $timeout.cancel(timer);
        }, 1000);
    };

    $scope.getContents = function (b) {

        if ($scope.currentView === null) {
            return;
        }

        //send to GA
        ga('send', 'event', $scope.currentGroup.title, b.media, b.title);

        if (b.media === "Website" || b.media === "Other") {
            window.open(b.mediaLink, '_blank');
            return;
        }
        if ($scope.homeBool) {
            $scope.currentView = 1;
        }

        $scope.newView = angular.copy($scope.currentView);
        $scope.currentView = null;
        var timer = $timeout(function () {
            $scope.newView++;
            $scope.currentView = $scope.newView;
            $scope.currentButton = angular.copy(b);
            if ($scope.currentButton.media === "Virtual Tour") {
                $scope.currentButton.mediaLink = "virtual-tours/" + $scope.currentButton.mediaLink;
            }
            if ($scope.currentButton.media === "PDF") {
                $scope.currentButton.mediaLink = $sce.trustAsResourceUrl($scope.currentButton.mediaLink);
            }

            $timeout.cancel(timer);
        }, 1000);
    };
});