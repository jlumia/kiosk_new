app.directive('customalert', function ($timeout, $sce) {
    return function (scope, element, attrs) {

        scope.$on('showalert', function (e, a) {
            scope.customAlertMessage = $sce.trustAsHtml(a);
            element.addClass("customAlertShow");
            var timer = $timeout(function () {
                element.removeClass("customAlertShow");
                $timeout.cancel(timer);
            }, 2500);
        });
    };
});